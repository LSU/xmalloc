#include <string.h>
#include <stdio.h>

#include "xmalloc.h"
static void *heap_start = NULL;

/** 当当前虚拟内存不足时,向堆申请,申请到大小为size的内存链接到last_ptr后面,**/
void *extend_head(p_block last_block,size_t size) {
	p_block new_block;
	if(size <= 0) {
		return (void *)NULL;
	}
	new_block = sbrk(0);	//得到首地址
	if(sbrk(BLOCK_SIZE + size) < (void*)0 )
		return (void *)NULL;
	new_block->size = size;
	new_block->next = NULL;
	new_block->prev = last_block;
	if(last_block) {
		new_block->next = last_block->next;
		last_block->next->prev = new_block;
		last_block->next = new_block;
	}
	/** 在free和realloc的时候,据此判断该内存块是否为该程序分配 **/
	new_block->ptr = new_block->data;	
	new_block->is_free = 0;				//分配空间后,就设置为非空闲状态。
	return new_block;
}

/**  从base节点开始遍历,找到一块大小符合的块,并修改last指向的块
	这里使用了最简单的快速适应算法,成功返回找到的块地址,否则返回NULL**/
p_block find_block(p_block *last_block,size_t size) {
	p_block base = heap_start;
	while(base) {
		if(base->size >= size && 1 == base->is_free) {
			*last_block = base;
			base = base->next;
		}
	}
	return base;
}

/** 当分配到的block较大时,需要相应的切割block **/
int split_block(p_block block,size_t size) {
	p_block new = NULL;
	if(!block) {
		return -1;
	}
	/**  **/
	new = (p_block)(((char *)block->data)+size);
	new->size = block->size - size - BLOCK_SIZE;
	block->size = size;
	new->ptr = new->data;
	new->is_free = 1;
	/** 拆分后的节点重新插入double LinkList **/
	block->prev = new;
	new->next = block->next;
	new->prev = block;
	block->next = new;
	return 0;
}
/**  **/
int xfusion(p_block block) {
	p_block tmp;
	if(block->next && block->next->is_free ==1) {
		block->size += block->next->size;
		tmp = block->next->next;
		tmp->prev = block;
		block->next = tmp;
	}
}

/** 分配可用的内存, 正常返回内存首地址,否则返回NULL。**/
void *xmalloc(size_t size) {
	int s;
	p_block b,last_block;
	if(size < 0) {
		return (void*)NULL;
	}
	/** 做响应对齐 **/
	s = alian_x(size);
	/** 第一次用该方法分配内存 **/
	if(!heap_start) {
		b = extend_head(NULL,s);
		if(b) {
			return b->data;
		}
		else {
			return (void*)NULL;
		}
	}
	last_block = heap_start;
	/** 找空间合适的块 **/
	b = find_block(&last_block,s);
	if(b) {
		if(b->size-BLOCK_SIZE-alien_size > s) {
			split_block(b,s);
		}
		b->is_free = 0;
	}
	else {
		/** last_block 为 b 的前驱　**/
		b = extend_head(last_block,s);
		if(!b) {
			return (void*)NULL;
		}
	}
	return (void *)(b->data);
}
/** 验证该内存是不是使用此方法(xcalloc,xrealloc,xmalloc)分配 **/
int xvaild(void *data) {
	p_block tmp;
	tmp = (p_block)((char*)data - BLOCK_SIZE);
	return tmp->ptr == data;	
}
int xfree(void *data) {
	int ret;
	p_block tmp;
	if(!data)
		return 0;
	if((ret = xvaild(data)) == 0)
	{
		return -1;
	}
	tmp = (p_block)((char*)data - BLOCK_SIZE);
	tmp->is_free = 1;
	/** 合并左右空闲的内存块 **/
	if((tmp->next && tmp->next != tmp && tmp->next->is_free ==1) || (tmp->prev && tmp->prev != tmp && tmp->prev->is_free ==1))
	{
		if((ret = xfusion(tmp)) <= 0) {
			return -1;
		}
	}
	return 1;
}

int main(int argc,char **argv) {
	//for(int i=0;i<=20;i++) {
	//	printf("align:%d==>%d  ,",i,(int)alian_x(i));
	//}
	printf("\n");
	char *str = xmalloc(10);	
	strcpy(str,"hello");
	printf("str:%s\n",str);
	return 0;
}