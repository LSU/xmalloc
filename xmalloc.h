#ifndef _XMALLOC_H
#define __XMALLOC_H

#include <stdlib.h>
#include <unistd.h>

/* 
	该结构体为内存管理的核心结构体,为了方便管理,该结构体必须(32/64)对齐,
	并且结构体内的成员需要连续,便于使用偏移就能查找到需要访问的成员,以至于
	可以在free内存的时候,知道该内存是否为xmalloc所申请的。
*/
typedef struct n_block *p_block;
struct n_block {
	size_t size;	//data域的长度
	p_block next;	//该内存段的后继
	p_block prev; 	//该内存段的前驱
	size_t is_free;	//标志位,是否为空闲内存段
	void *ptr;		//标志位,给free的时候判断是否为xmalloc申请的内存
	char data[1];	//数据域,真正提供给申请的应用程序使用的指针
};

/*提前定义BLOCK大小,在计算机体系架构不同和字节对齐等方面提供兼容*/
#define BLOCK_SIZE sizeof(struct n_block)
/** 对齐 **/
#define alien_size sizeof(size_t)
#define alian_x(x) (alien_size+(((x-1)/alien_size)*alien_size))

#endif