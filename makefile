bin = bin
#cmmH = /home/simon/opensource/include
#cmmL = /home/simon/opensource/lib
GCC = gcc -Wall -g

all:testLimit testMalloc test_thread_max testAlign testEV testEpollET testEpollET2 testEpollLT testHostByName testStr
#	make clean

test_thread_max:
	$(GCC) test_thread_max.c -lpthread -o $(bin)/$@
testEV:
	$(GCC)  testEV.c   -I $(cmmH)  -L $(cmmL) -lpthread -lev -lm -o $(bin)/$@
testEpollET:
	$(GCC)  testEpollET.c   -I $(cmmH)  -L $(cmmL) -lpthread -lev -lm -o $(bin)/$@
testEpollET2:
	$(GCC)  testEpollET2.c   -I $(cmmH)  -L $(cmmL) -lpthread -lev -lm -o $(bin)/$@
testEpollLT:
	$(GCC)  testEpollLT.c   -I $(cmmH)  -L $(cmmL) -lpthread -lev -lm -o $(bin)/$@
testMalloc:
	$(GCC)  testMalloc.c   -I $(cmmH)  -L $(cmmL) -lpthread -lev -lm -o $(bin)/$@
testHostByName:
	$(GCC)  testHostByName.c   -I $(cmmH)  -L $(cmmL) -lpthread -lev -lm -o $(bin)/$@
testStr:
	$(GCC)  testStr.c   -I $(cmmH)  -L $(cmmL) -lpthread -lev -lm -o $(bin)/$@
testAlign:
	$(GCC)  testAlign.c   -I $(cmmH)  -L $(cmmL) -lpthread -lev -lm -o $(bin)/$@
testLimit:
	$(GCC)  testLimit.c   -I $(cmmH)  -L $(cmmL) -lpthread -lev -lm -o $(bin)/$@
clean:
	rm bin/*
	
